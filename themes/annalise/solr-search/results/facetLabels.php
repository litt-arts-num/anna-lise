<?php 

$facetLabels = [
  "itemtype",
  "r_alisateur_r_alisatrice_item_type_metadata_s",
  "pays_de_production_item_type_metadata_s",
  "ann_e_de_production_item_type_metadata_s",
  "auteur_autrice_du_document_item_type_metadata_s",
  "auteur_autrice_de_l_ouvrage_item_type_metadata_s",
  "sous_la_direction_de_item_type_metadata_s",
  "type_de_texte_item_type_metadata_s",
  "type_d_analyse_item_type_metadata_s",
  "revue_item_type_metadata_s",
  "collection_item_type_metadata_s",
  "ann_e_de_parution_item_type_metadata_s",
  "titre_de_l_ouvrage_item_type_metadata_s",
  "outil_proc_d_param_tre_filmique_item_type_metadata_s",
  "motif_th_me_item_type_metadata_s",
  "concept_invention_th_orique_item_type_metadata_s",
  "personne_physique_personne_morale_item_type_metadata_s",
  "personnage_item_type_metadata_s",
  "genre_cin_matographique_item_type_metadata_s",
  "lien_avec_d_autres_arts_item_type_metadata_s",
  "oeuvre_non_cin_matographique_item_type_metadata_s"
];

//
// Some Facets might be activated but intentionally not displayed : those are used in combination with the SearchByMetadato solr results ta plugin in our (cheeky) Solr compatibility mode.
// if not activated as facets the links to all items sharing the same metadatas won't be able redirect to solr results (though it can redirect to Omeka's default search engine and default search display results)
// For safety and clarity, I write them down here and comment them out :
//
// "num_ro_item_type_metadata_s"
//
// Other facets are simply not activated :
//
// "pour_citer_item_type_metadata_s"
// "titre_fran_ais_item_type_metadata" (for films)
// "notes_item_type_metadata"
//
// Omeka categories facets :
//
// "tag"
// "collection"
// "resulttype" (Item / Simple Page / File / collection , etc. )
// "featured"
//
// All dublin core facets follow the model : creator_dublin_core <prop_name>__dublin_core


?>