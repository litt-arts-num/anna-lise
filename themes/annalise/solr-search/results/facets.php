<?php 

include "facetLabels.php";

function stripSolrFormatting($value) {
    // We cannot excape html or '(' ')' form solr original queries : the searched strings contain those, if we ignore them, we have no match
    // but store the last textQuery and to compare the text query to the facets values, we strip all strings
    // this function is used :
    $strippedValue = str_replace(['"', '(', ')'], '', (strip_formatting($value)));
    $strippedValue = strtolower($strippedValue);

    return $strippedValue;
}

// Added for a better comparison and match of the text wuery with the facets values (utf8 Maj Chars such as É were otherwise never matched)
function to_ascii($value) {
  $unwanted_array = array(    
    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A',
    'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O',
    'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B',
    'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
    'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o',
    'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
    $str = strtr( $value, $unwanted_array );
    $str = strtolower($str);
    return $str;
}




// Hardcoded display order for facets (sloppy...)
$retrievedFacets = get_object_vars($results->facet_counts->facet_fields);

$orderedFacets = [];
foreach ($facetLabels as $label) {
  $orderedFacets[$label] = $retrievedFacets[$label];
}

// delete the empty facets from this list (if a $retrievedFacet is empty we don't wanna loop on it)
foreach ($orderedFacets as $key => $value) {
    if ((!(array)$value)) {
        unset($orderedFacets[$key]);
    }
}
?>

<div id="left-main-content">
  <div id="solr-facets">
    <h5 style="margin-left:10px;" >
      <b><?= __('Filtres disponibles') ?></b>
    </h5>
    <!-- ACCORDEON -->
    <div class="accordion accordion-flush" id="accordion-facets">
    <?php 
      // split the SOLR text query into an array of words 
      $words = explode(' ', to_ascii(stripSolrFormatting($textQuery)));
      
      // Now we can loop in the facets
      foreach ($orderedFacets as $name => $facets): 
                
        if ($textQuery != "") {
          $facetArrayText = [];
          // we push each distinct value of the facet in a new array 
          foreach (get_object_vars($facets) as $text => $hitNb) {
              array_push($facetArrayText, to_ascii(stripSolrFormatting($text)));
          }
        
          $foundKeys = $facetArrayText;
          $i = 0;
          // and we loop up in this array looking for each possible word (we stop when no word are left to match)
          while ($i < sizeof($words)) {
            $test = preg_grep("/$words[$i]/i", $foundKeys);
            if (!empty($test)) {
                $foundKeys = $test;
                $i++;
            } else {
                break;
            }
          }
          // if we've found all the words of the text query : we will give a special css class to the facet 
          if ($i == sizeof($words)) {
            $accordionCss = ['true', 'accordion-collapse collapse show'] ;
            $matchingVal = $foundKeys;

          } else {
            $accordionCss = ['false', 'accordion-collapse collapse'] ;
            $matchingVal = null;
          }
        } elseif ($name == "itemtype") {
          // itemtype facet should always be opened
          $accordionCss = ['true', 'accordion-collapse collapse show'] ;
          $matchingVal = null;
        
        } else {
          // without any text query to match, all other facets should be closed by default.
          $accordionCss = ['false', 'accordion-collapse collapse'] ;
      }

      $label = SolrSearch_Helpers_Facet::keyToLabel($name);
      // accordid added with bootstrap
      $accordId = text_to_id($label);
    ?>

      <!-- FACETTE -->
      <div class="accordion-item">

        <!-- HEADER FACETTE -->
        <h2 class="accordion-header" id="<?= 'panel-heading-' . $accordId  ?>">
          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="<?= '#panel-' . $accordId ?>" aria-expanded="<?=$accordionCss[0] ?>" aria-controls="<?= 'panel-' . $accordId ?>">
            <?=$label?>
              (<?= sizeof(get_object_vars($facets)) /*for each facet category we also display the count of different value*/?>)
          </button>
        </h2>

        <!-- BODY FACETTE -->
        <div id="<?='panel-'.$accordId ?>" class="<?=$accordionCss[1] ?>">
          <div class="accordion-body">

            <!-- FILTRE ET TRI D'UNE FACETTE -->
            <div class="d-flex justify-content-between align-items-start filters" data-target="<?= $accordId ?>">
              <input class="form-control form-control-sm mb-2 me-2 filter-facette" placeholder="<?= 'filtrer ' . $label ?>" data-target="<?= $accordId ?>" type="text" />
              <div class="col">
                <div class="btn-group btn-group-sm" role="group">
                  <button type="button"  data-target="<?= $accordId ?>" data-criteria="value" data-order="asc" class="btn btn-sm btn-outline-dark sort">
                    A-Z
                  </button>
                  <button type="button" data-target="<?= $accordId ?>" data-criteria="count" data-order="desc" class="btn btn-sm btn-outline-dark sort">
                    1-9
                  </button>
                </div>
              </div>
            </div>

            <!-- VALEURS D'UNE FACETTE -->
            <ul class="list-group list-group-flush facette" data-facette="<?= $accordId ?>">
              <?php foreach ($facets as $value => $count):
                    // $ascii_val =  to_ascii(stripSolrFormatting($value));
                    // var_dump($ascii_val);
                    //var_dump(strtolower(stripSolrFormatting($value)));

                $matchingFacetClass = ($matchingVal && in_array(to_ascii(stripSolrFormatting($value)), $matchingVal)) ? 'facet-match' : '';?>
                <li data-count="<?=$count?>" data-value="<?=$value?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-start <?= $matchingFacetClass; ?>">
                  <a href="<?= SolrSearch_Helpers_Facet::addFacet($name, $value); ?>" class="facet-value">
                    <?=$value?>
                  </a>
                  <span class="badge bg-dark rounded-pill">
                    <?=$count?>
                  </span>
                </li>
              <?php endforeach; ?>
            </ul>

          </div> <!-- end of accordeon body -->
        </div> <!-- end of accordeon body -->
        
      </div>  <!-- FIN D'UNE FACETTE -->

    <?php endforeach; // FIN DES FACETTES?>
    </div> <!-- end of accordeon -->
  </div>
</div>
