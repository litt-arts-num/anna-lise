<div id="solr-sort">
    <form method="get" name="solrsort">
        <?php  foreach ($query as $key => $value): ?>
            <?php if (!is_array($value) && $key !== 'sort'): ?>
                <?php echo $this->formHidden($key, $value); ?>
            <?php endif; ?>
        <?php endforeach; ?>


        <button style="display:none;" type="submit"><?php //echo __('Sort');?>Trier par </button>

        <label>
            <?php
            // echo __('Sort by');
            $sort = isset($query['sort']) ? $query['sort'] : null;
            echo $this->formSelect('sort', $sort, null, $sortOptions);
            ?>
        </label>
    </form>
</div>
