<h5 class="card-title">
    <a href="<?=$url;?>" class="result-title">
      <?= (empty($title)) ? '<i>'. __('Untitled').'</i>' : $title ?>
      <?php if (metadata($item, ['Item Type Metadata', 'Titre français'])): ?>
        <span class="card-subtitle mb-2 text-muted" style="text-decoration: none !important;">
          <?= "(" . strip_formatting(metadata($item, ['Item Type Metadata', 'Titre français']), '<em>'). ")"  ;?>
        </span>
      <?php endif; ?>
    </a>
</h5>
