<?php
  $url = SolrSearch_Helpers_View::getDocumentUrl($doc);
  // Retrieves Record Type using its ID
  $item = get_db()->getTable($doc->model)->find($doc->modelid);
  $itemtype = is_array($doc->itemtype) ? $doc->itemtype[0] : $doc->itemtype;
  $title = is_array($doc->title) ? $doc->title[0] : $doc->title;
 ?>

 <div class="card border-dark m-3 result-item" data-type="<?= text_to_id($itemtype) ?>">
   <div class="card-body py-2">
    <?php
    if (get_class($item) == "Item") {
        echo $this->partial('/results/item/title.php', ['url' => $url, 'title' => $title, 'item' => $item]);
        if ($itemtype == "Films"):
        echo $this->partial('/results/item/type-movie.php', ['item' => $item]); else: //elseif ($itemtype == "Analyses" or $itemtype == "Analyses d'analyses" or $itemtype == "Théorie de l'analyse"):
        echo $this->partial('/results/item/type-other.php', ['item' => $item]);
        endif;
        // Lists Solr Matches in the record's metadatas, dependending of its type.
        if (get_option('solr_search_hl')):
          echo $this->partial('/results/item/highlights.php', ['doc' => $doc,'textQuery' => $textQuery,'results' => $results]);
        endif;
    }

    ?>
  </div>
</div>
