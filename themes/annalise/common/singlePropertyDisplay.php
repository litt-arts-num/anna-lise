  <?php
    if (propertyExists($elementsForDisplay, $metadataSet, $metadata)): ?>

    <?php if (($metadata == "Auteur, autrice du document")
                or ($metadata == "Auteur, autrice de l'ouvrage")
                or ($metadata == "Sous la direction de")
                or ($metadata == "Personne physique, personne morale")
                or ($metadata == "Réalisateur, réalisatrice")
                or ($metadata == "Titre de l'ouvrage")
                or ($metadata == "Oeuvre non-cinématographique")): ?>

            <?php // if SolrSearchByMetadata is active, it will handle links on metadata properties values by itself except for those ABOVE
                  // For those we search values across several kinds of metadata :
                  // the simplest solution is to search their values as full-text queries across all metadata properties.
            if (plugin_is_active('SolrSearchByMetadata')): ?>

                <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
                    <h3><?php getLabel($elementsForDisplay, $metadataSet, $metadata) ?></h3>
                    <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>
                      <div class="element-text">
                        <div>
                          <a href="<?php echo '/solr-search?q=' . '%22' . strip_formatting($text, "<em>") . '%22' ;?>">
                              <?php echo strip_formatting($text, "<em>"); ?>
                          </a>
                        </div>
                      </div>
                    <?php endforeach; ?>
                </div>
        <?php endif; ?>
    <?php else: ?>
        <div id="<?php labelToId($metadataSet, $metadata); ?>" class="element">
            <h3><?php getLabel($elementsForDisplay, $metadataSet, $metadata) ?></h3>
            <?php foreach ($elementsForDisplay[$metadataSet][$metadata]["texts"] as $text): ?>
                <div class="element-text">
                  <?php echo(__($text)); ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
