
<?php // If no relations at all, we skip all this code ;
if (!$subjectRelations && !$objectRelations):
    return;
endif;

// S'il y a bien des relations avec d'autres objets, on les ramène toutes dans le même array, puisque la direction n'a pas d'importance
$whateverRelations = [];

if ($subjectRelations) {
    foreach ($subjectRelations as $rel) {
        $currentRelArray['relatedItem'] = $rel["object_item_id"];
        $currentRelArray['relatedItemTitle'] = $rel["object_item_title"];
        $currentRelArray['relationType'] = $rel["relation_text"];
        // on en profite pour ajouter le type de l'item relié à l'array
        $targetItem = get_record_by_id('Item', $rel['object_item_id']);
        $currentRelArray['relatedItemClass'] = metadata($targetItem, 'item_type_name');
        array_push($whateverRelations, $currentRelArray);
    }
}

if ($objectRelations) {
    foreach ($objectRelations as $rel) {
        $currentRelArray['relatedItem'] = $rel["subject_item_id"];
        $currentRelArray['relatedItemTitle'] = $rel["subject_item_title"];
        $currentRelArray['relationType'] = $rel["relation_text"];
        // on en profite pour ajouter le type de l'item relié à l'array
        $targetItem = get_record_by_id('Item', $rel['subject_item_id']);
        $currentRelArray['relatedItemClass'] = metadata($targetItem, 'item_type_name');
        array_push($whateverRelations, $currentRelArray);
    }
}

// En fait, autant trier de suite les différentes relations en fonction de leurs types :
$principalRelatedFilms = [];
$secondaryRelatedFilms = [];
$principalRelatedAnalyses = [];
$secondaryRelatedAnalyses = [];
$principalRelatedAnalysesDanalyses = [];
$secondaryRelatedAnalysesDanalyses = [];

foreach ($whateverRelations as $rel) {
    if ($rel['relatedItemClass'] == "Films" and $rel['relationType'] == "principal") {
        array_push($principalRelatedFilms, $rel);
    }
    if ($rel['relatedItemClass'] == "Films" and $rel['relationType'] == "secondary") {
        array_push($secondaryRelatedFilms, $rel);
    }
    if ($rel['relatedItemClass'] == "Analyses" and $rel['relationType'] == "principal") {
        array_push($principalRelatedAnalyses, $rel);
    }
    if ($rel['relatedItemClass'] == "Analyses" and $rel['relationType'] == "secondary") {
        array_push($secondaryRelatedAnalyses, $rel);
    }
    if ($rel['relatedItemClass'] == "Analyses d&#039;analyses" and $rel['relationType'] == "principal") {
        array_push($principalRelatedAnalysesDanalyses, $rel);
    }
    if ($rel['relatedItemClass'] == "Analyses d&#039;analyses" and $rel['relationType'] == "secondary") {
        array_push($secondaryRelatedAnalysesDanalyses, $rel);
    }
}
?>
<!-- AFFICHAGE DES RELATIONS SI L'ITEM COURANT EST UN FILM -->
<?php if (metadata('item', 'item_type_name') == "Films"): ?>

      <?php if ($principalRelatedAnalyses or $secondaryRelatedAnalyses or $principalRelatedAnalysesDanalyses or $secondaryRelatedAnalysesDanalyses): ?>
        <div id="relations-to-analyses-from-films">
                <?php if ($principalRelatedAnalyses  or $principalRelatedAnalysesDanalyses): ?>
                  <h2><?php echo 'Film analysé dans ';?></h2>
                  <table>
                      <?php foreach ($principalRelatedAnalyses as $rel): ?>
                        <tr>
                             <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                        </tr>
                      <?php endforeach; ?>
                      <?php  foreach ($principalRelatedAnalysesDanalyses as $rel): ?>
                        <tr>
                             <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                        </tr>
                      <?php endforeach; ?>
                  </table>
                <?php endif; ?>


                <?php if ($secondaryRelatedAnalyses  or $secondaryRelatedAnalysesDanalyses): ?>
                  <h2><?php echo 'Film abordé dans :';?></h2>
                  <table>
                  <?php  foreach ($secondaryRelatedAnalyses as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
                  <?php  foreach ($secondaryRelatedAnalysesDanalyses as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
                  </table>
                <?php endif; ?>
        </div>
      <?php endif; ?>

<!-- Relation de film à film. N'est pas sensée exister, mais je vous connaît, vous allez essayer d'en mettre quand même !  -->
      <?php if ($principalRelatedFilms or $secondaryRelatedFilms): ?>
        <div id="relations-to-films-from-films">
            <h2><?php echo 'Films en relation';?></h2>
            <table>
                  <?php  foreach ($principalRelatedFilms as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
                  <?php  foreach ($secondaryRelatedFilms as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
            </table>
        </div>
      <?php endif; ?>

<?php endif; ?>

<!-- AFFICHAGE DES RELATIONS SI L'ITEM COURANT EST UNE ANALYSE -->

<?php if (metadata('item', 'item_type_name') == "Analyses"): ?>

  <?php if ($principalRelatedFilms or $secondaryRelatedFilms): ?>
    <div id="relations-to-films-from-analyses">
      <?php if ($principalRelatedFilms): ?>
          <h2><?php echo 'Films analysés';?></h2>
            <table>
                  <?php  foreach ($principalRelatedFilms as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
            </table>
      <?php endif; ?>
      <?php if ($secondaryRelatedFilms): ?>
          <h2><?php echo 'Films abordés';?></h2>
          <table>
              <?php  foreach ($secondaryRelatedFilms as $rel): ?>
                <tr>
                     <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                </tr>
              <?php endforeach; ?>
        </table>
      <?php endif; ?>
    </div>
<?php endif;?>

  <?php if ($principalRelatedAnalyses or $secondaryRelatedAnalyses or $principalRelatedAnalysesDanalyses or $secondaryRelatedAnalysesDanalyses): ?>
    <div id="relations-to-analysesdanalyses">

            <?php if ($principalRelatedAnalysesDanalyses): ?>
              <h2><?php echo 'Analyses de cette analyse';?></h2>
                <table>
                  <?php  foreach ($principalRelatedAnalysesDanalyses as $rel): ?>
                    <tr>
                         <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                    </tr>
                  <?php endforeach; ?>
                </table>
            <?php endif; ?>

            <?php if ($secondaryRelatedAnalyses  or $secondaryRelatedAnalysesDanalyses): ?>
              <h2><?php echo 'Analyses abordant cette analyse';?></h2>
                <table>
              <?php  foreach ($secondaryRelatedAnalysesDanalyses as $rel): ?>
                <tr>
                     <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                </tr>
              <?php endforeach; ?>
            </table>
            <?php endif; ?>
    </div>
  <?php endif; ?>

<?php endif; ?>
 <!-- Fin des relations pour les Analyses -->

 <!-- AFFICHAGE DES RELATIONS SI L'ITEM COURANT EST UNE ANALYSE D'ANALYSES -->

 <?php if (metadata('item', 'item_type_name') == "Analyses d&#039;analyses" or metadata('item', 'item_type_name') == "Théorie de l&#039;analyse"): ?>



   <?php if ($principalRelatedAnalyses or $secondaryRelatedAnalyses or $principalRelatedAnalysesDanalyses or $secondaryRelatedAnalysesDanalyses): ?>
     <div id="relations-to-analyses-from-analysesdanalyses">
             <?php if ($principalRelatedAnalyses  or $principalRelatedAnalysesDanalyses): ?>
               <h2><?php echo 'Analyses analysées';?></h2>
               <table>
                <?php  foreach ($principalRelatedAnalyses as $rel):?>
                 <tr>
                      <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']);?>"><?php echo $rel['relatedItemTitle'];?></a></td>
                 </tr>
               <?php endforeach; ?>
               <?php  foreach ($principalRelatedAnalysesDanalyses as $rel): ?>
                 <tr>
                      <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                 </tr>
               <?php endforeach; ?>
                </table>
             <?php endif; ?>

             <?php if ($secondaryRelatedAnalyses  or $secondaryRelatedAnalysesDanalyses): ?>
               <h2><?php echo 'Analyses abordées';?></h2>
                 <table>
                <?php  foreach ($secondaryRelatedAnalyses as $rel):?>
                 <tr>
                      <td>Item: <a href="<?php  echo url('items/show/' . $rel['relatedItem']);?>"><?php echo $rel['relatedItemTitle'];?></a></td>
                 </tr>
               <?php endforeach;?>
               <?php  foreach ($secondaryRelatedAnalysesDanalyses as $rel): ?>
                 <tr>
                      <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                 </tr>
               <?php endforeach; ?>
                </table>
             <?php endif; ?>

     </div>
   <?php endif; ?>

   <?php if ($principalRelatedFilms or $secondaryRelatedFilms): ?>



     <div id="relations-to-films-from-analyses">
             <?php  if ($principalRelatedFilms): ?>
               <h2><?php echo 'Films Analysés';?></h2>
               <table>
                 <?php  foreach ($principalRelatedFilms as $rel): ?>
                   <tr>
                        <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                   </tr>
                 <?php endforeach; ?>
               </table>
             <?php endif; ?>

             <?php  if ($secondaryRelatedFilms): ?>
               <h2><?php echo 'Films abordés';?></h2>
               <table>
                 <?php  foreach ($secondaryRelatedFilms as $rel): ?>
                   <tr>
                        <td>Item: <a href="<?php echo url('items/show/' . $rel['relatedItem']); ?>"><?php echo $rel['relatedItemTitle']; ?></a></td>
                   </tr>
                 <?php endforeach; ?>
               </table>
             <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php endif; ?> <!-- Des Relations des analyses d'Analyses -->
