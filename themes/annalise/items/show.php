<meta charset="UTF-8">

<?php echo head(array('title' => metadata('item', array('Dublin Core', 'Title')),'bodyclass' => 'items show')); ?>

<div id="left-content">
  <div id="left-header">
      <?php  echo search_form();?>
  </div>

  <h1 id="<?php // Pour une coloration en fonction type, un id à partir de l'item type (et style associé à l'id d'origine transposé en classe)
            echo text_to_id(str_replace(array("&#039;", "é"), "", metadata('item', 'item_type_name'))); ?>">
      <?php echo metadata('item', array('Dublin Core', 'Title'));?>
  </h1>

  <?php if (metadata('item', 'item_type_name') != "Films"):?>
      <div id="relations">
        <?php // hook qui déclenche le plugin itemRelations, mais le template par défaut est remplacé par un fichier du même nom dans /themes/annalise/common/item-relations-show.php
         fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));?>
      </div>
  <?php endif; ?>


  <div class ="left-main-content" id="<?php // id pour background color en f° du type de l'item courant
  echo text_to_id(str_replace(array("&#039;", "é"), "", metadata('item', 'item_type_name'))); ?>">


    <?php // The following returns all of the files associated with an item Replaced bellow in div @id=contentfull
    /*if ((get_theme_option('Item FileGallery') == 1) && metadata('item', 'has files')): ?>
      <div id="itemfiles" class="element">
          <h2><?php echo __('Files'); ?></h2>
          <?php echo item_image_gallery(); ?>
      </div>
    <?php endif; */?>


    <?php // If the item belongs to a collection, the following creates a link to that collection.
          // Hidden in CSS
          if (metadata('item', 'Collection Name')): ?>
      <div id="collection" class="element" >
          <h2><?php echo __('Collection'); ?></h2>
          <div class="element-text"><p><?php echo link_to_collection_for_item(); ?></p></div>
      </div>
    <?php endif; ?>

    <?php // The following prints a list of all tags associated with the item
          if (metadata('item', 'has tags')): ?>
      <div id="item-tags" class="element">
          <h2><?php echo __('Tags'); ?></h2>
          <div class="element-text"><?php echo tag_string('item'); ?></div>
      </div>
    <?php endif;?>

    <?php // Custom Metadata display templates replacing  the function all_element_texts('item'). 2 templates : one for Films / one for the many kinds of Analyses
        if (metadata('item', 'item_type_name') == "Films") {
            echo all_element_texts('item', array('partial' => 'common/films-record-metadata.php'));
        } elseif (metadata('item', 'item_type_name') == "Analyses"
              || metadata('item', 'item_type_name') == "Analyses d&#039;analyses"
              || metadata('item', 'item_type_name') == "Théorie de l&#039;analyse") {
            echo all_element_texts('item', array('partial' => 'common/analyses-record-metadata.php'));
        }
             ?>


       <?php if (metadata('item', 'item_type_name') == "Films"):?>
           <div id="relations">
             <?php // hook qui déclenche le plugin itemRelations, mais le template par défaut est remplacé par un fichier du même nom dans /themes/annalise/common/item-relations-show.php
             fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item));?>
           </div>
       <?php endif; ?>

    <!-- Citation déplacée au bas du panneau latéral -->
    <div id="item-citation" class="element">
        <h2> Pour citer cette page <?php //echo __('Citation');?></h2>
        <div class="element-text"><?php echo metadata('item', 'citation', array('no_escape' => true)); ?></div>
    </div>


  </div> <!-- FIN DE   <div id="left-main-content">  -->
</div> <!--FIN DE <div id="left-content"> -->

<!-- MAIN SCREEN AND PDF DISPLAY -->
<div id="content-full" style="/*display:block;*/"> <!-- Affiche les fichiers liés à l'item : les fameux PDF-->

<!-- NAVBAR of the main screen -->
      <?php if ((get_theme_option('Item FileGallery') == 0) && metadata('item', 'has files')) {
                 echo $this->partial('/items/fileDisplay.php', ['item' => $item]);
             }
      ?>
</div>

<?php echo foot(); ?>
