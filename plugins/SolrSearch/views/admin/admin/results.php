<?php

/**
 * @package     omeka
 * @subpackage  solr-search
 * @copyright   2012 Rector and Board of Visitors, University of Virginia
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 */

?>

<!-- Repairs Form display bug with minimal effort -->
<style>
#fieldset-fields {
  min-width: 600px !important;}
</style>


<?php queue_js_file('vendor/tinymce/tinymce.min'); ?>
<?php queue_css_file('style'); ?>

<?php echo head(array(
  'title' => __('Solr Search | Results Configuration')
)); ?>

<?php echo $this->partial('admin/partials/navigation.php', array(
  'tab' => 'results'
)); ?>

<div id="primary">
  <h2><?php echo __('Results Configuration') ?></h2>
	<?php echo flash(); ?>
	<?php echo $form; ?>


<!-- Insert here -->

  <p> ho </p>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        Omeka.wysiwyg();
    });
</script>



<?php echo foot(); ?>
